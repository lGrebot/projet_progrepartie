import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ServiceTraitementTexte {
	public void afficherMap() throws RemoteException;
	public static void repartirLignesProxy(ArrayList<String> a, int nbProxy);
}
