import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.NotBoundException;

public class MainNoeud {
	
    public static void main (String args[])throws RemoteException, NotBoundException{

    String serveur="localhost";
    int port=1099;


    if( args.length > 0 )
        serveur=args[0];
    if( args.length > 1 )
        port=Integer.parseInt(args[1]);
    
  
    Registry annuaire = LocateRegistry.getRegistry(serveur, port);    
    ServiceDistributeur sd  = (ServiceDistributeur) annuaire.lookup("distributeur");
    
     

	TraitementTexte tt = new TraitementTexte(sd);
	ServiceTraitementTexte stt = (ServiceTraitementTexte) UnicastRemoteObject.exportObject(tt, 0);
	sd.enregistrerClient(stt);
//	sd.convertirTexte(args[1]); (afficher map)
	
    }
}
