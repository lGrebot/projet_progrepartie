import java.io.File;
import java.io.FileNotFoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Distributeur implements ServiceDistributeur {
	private ArrayList<ServiceTraitementTexte> proxy = new ArrayList<>();
	public int n = 0;

	public void enregistrerClient(ServiceTraitementTexte stt) throws RemoteException {
		proxy.add(stt);
		System.out.println(stt);
	}

	// associe le string decoup� dans la distribution du message
	public void distribuerMessage(String s) throws RemoteException {
		
		ArrayList<Map<String, Integer>> retours  ;
		
		
		for (ServiceTraitementTexte stt : proxy) {
			Map<String, Integer> result = stt.afficherMap(repartirLignesProxy(a, idProxy););
			retours.add(result);
		}
		
		
		//Reconstruire Map globale avec les resultats des maps dans retour
	}

	public static ArrayList<String> convertirTexte(String adresse) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(adresse));
		ArrayList<String> ligne = new ArrayList<String>();
		while (sc.hasNextLine()) {
			ligne.add(sc.nextLine());
		}
		return ligne;
	}

	public static void repartirLignesProxy(ArrayList<String> a, int idProxy) {
		int nbLignes = a.size();
		int min = (int) nbLignes / proxy.size();
		int res = nbLignes % proxy.size();
		ArrayList<String> tmp;
		int iterator = 0;
		for (int i = 0; i < proxy.size(); i++) {
			tmp = new ArrayList<String>();
			for (int j = 0; j < min; j++) {
				tmp.add(a.get(iterator));
				iterator++;
			}
			if (i<res) {
				tmp.add(a.get(a.size()-(i+1)));
			}
			System.out.println(tmp);
		}
	}
}
