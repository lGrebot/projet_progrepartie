import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

public class Central {

   public static void main (String args[])throws RemoteException{
    
       Distributeur d = new Distributeur();

    ServiceDistributeur sd =(ServiceDistributeur) UnicastRemoteObject.exportObject(d, 0);
    Registry annuaire = LocateRegistry.getRegistry();
    annuaire.rebind("distributeur", sd);
    }
}
