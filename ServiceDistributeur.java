import java.rmi.RemoteException;
import java.io.FileNotFoundException;
import java.rmi.Remote;

public interface ServiceDistributeur extends Remote{
    public void enregistrerClient(ServiceTraitementTexte stt) throws RemoteException;
    
    public void distribuerMessage(String s) throws RemoteException;
    
    public String[]  (String adresse) throws FileNotFoundException;
}
