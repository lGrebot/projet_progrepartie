package projetbis;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Principale {

	public static ArrayList<String> convertirTexte(String adresse) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(adresse));
		ArrayList<String> ligne = new ArrayList<String>();
		while (sc.hasNextLine()) {
			ligne.add(sc.nextLine());
		}
		return ligne;
	}

	public static void repartirLignesProxy(ArrayList<String> a, int nbProxy) {
		HashMap<Integer, ArrayList<String>> map = new HashMap<Integer, ArrayList<String>>();
		int nbLignes = a.size();
		int min = (int) nbLignes / nbProxy;
		int res = nbLignes % nbProxy;
		ArrayList<String> tmp;
		int iterator = 0;
		for (int i = 0; i < nbProxy; i++) {
			tmp = new ArrayList<String>();
			for (int j = 0; j < min; j++) {
				tmp.add(a.get(iterator));
				iterator++;
			}
			if (i<res) {
				tmp.add(a.get(a.size()-(i+1)));
			}
			System.out.println(tmp);
		}
	}

	public static void main(String[] args) throws FileNotFoundException {
		ArrayList<String> res = convertirTexte("texte.txt");
		repartirLignesProxy(res, 3);
	}
}
